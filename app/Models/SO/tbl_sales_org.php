<?php

namespace App\Models\SO;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_sales_org extends Model {
    use HasFactory;

    public $table = 'tbl_sales_org';
}
